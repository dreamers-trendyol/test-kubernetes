package com.dreamerstrendyol.marketplace.Exceptions;

import com.couchbase.client.core.error.AmbiguousTimeoutException;
import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.core.error.UnambiguousTimeoutException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.context.request.WebRequest;

import java.util.Date;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(BalanceIsNotEnoughException.class)
    public ResponseEntity<Object> balanceIsNotEnoughException(BalanceIsNotEnoughException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("BalanceIsNotEnoughException",
                exception.getMessage(), request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({DocumentNotFoundException.class})
    public ResponseEntity<Object> documentNotFoundException(DocumentNotFoundException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("DocumentNotFoundException",
                exception.getMessage(), request.getDescription(true), new Date());
        System.out.println("Exception Handler");
        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({AmbiguousTimeoutException.class})
    public ResponseEntity<Object> ambiguousTimeoutException(AmbiguousTimeoutException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("AmbiguousTimeoutException",
                exception.getMessage(), request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({UnambiguousTimeoutException.class})
    public ResponseEntity<Object> ambiguousTimeoutException(UnambiguousTimeoutException exception, WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("UnambiguousTimeoutException",
                exception.getMessage(), request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({NullPointerException.class})
    public ResponseEntity<Object> nullPointerException(WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("NullPointerException",
                "You trying to make request null pointer target",
                request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler({HttpMessageNotReadableException.class})
    public ResponseEntity<Object> httpMessageNotReadableException(WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("HttpMessageNotReadableException",
                "Request can't be accepted.",
                request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ResourceAccessException.class})
    public ResponseEntity<Object> resourceAccessException(WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("ResourceAccessException",
                "Server can't access data please try again.",
                request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity<Object> methodArgumentNotValidException(WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("MethodArgumentNotValidException",
                "Some Fields need to ",
                request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({HttpMediaTypeNotSupportedException.class})
    public ResponseEntity<Object> httpMediaTypeNotSupportedException(WebRequest request) {
        ExceptionDetails details = new ExceptionDetails("MethodArgumentNotValidException",
                "Please try to change request header as application/JSON",
                request.getDescription(true), new Date());

        return new ResponseEntity<>(details, HttpStatus.NOT_FOUND);
    }
}
