package com.dreamerstrendyol.marketplace.Services;

import com.dreamerstrendyol.marketplace.Contracts.Request.CreateGameRequest;
import com.dreamerstrendyol.marketplace.Contracts.Response.TopGameResponse;
import com.dreamerstrendyol.marketplace.Contracts.Response.UpcomingGameResponse;
import com.dreamerstrendyol.marketplace.Exceptions.BalanceIsNotEnoughException;
import com.dreamerstrendyol.marketplace.Models.*;
import com.dreamerstrendyol.marketplace.Repositories.MarketRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class MarketService {

    @Autowired
    RestTemplate getRestTemplate;

    @Autowired
    private KafkaSender sender;

    @Autowired
    private KafkaConsumer consumer;

    @Autowired
    private MarketRepository marketRepository;

    public List<Game> getAllGames() {
        sender.sendMessage("Deneme", "bootchamp1");
        sender.sendMessage("Merhaba", "bootchamp2");
        return marketRepository.getAllGames();
    }

    public String createGame(CreateGameRequest createGameRequest) {
        Game game = new Game(createGameRequest.getGameName(), createGameRequest.getGameDescription(),
                createGameRequest.getPublisherCompany(), createGameRequest.getGenre(), createGameRequest.getInternetSite(),
                createGameRequest.getPublisherSite(), createGameRequest.getPrice(), createGameRequest.getGameIcon(),
                createGameRequest.getHasAdultContent(), createGameRequest.getReleaseDate(), createGameRequest.getTags(),
                createGameRequest.getPlatform(), createGameRequest.getGameMediaContainer(), createGameRequest.getSystemRequirements(),
                createGameRequest.getLanguages());
        return marketRepository.createGame(game);
    }

    public Game getGameById(String gameId) {
        System.out.println("Hello from service");
        return marketRepository.getGameById(gameId);
    }

    public void deleteGame(String gameId) {
        marketRepository.deleteGame(gameId);
    }

    public Double getPrice(String gameId) {
        return marketRepository.getPrice(gameId);
    }

    public int getSalesCount(String gameId) {
        return marketRepository.getSalesCount(gameId);
    }

    public List<Review> getReviews(String gameId) {
        return marketRepository.getReviews(gameId);
    }

    public String addReview(String gameId, Review review) {
        Game game = marketRepository.getGameById(gameId);
        review.setCreatedAt(LocalDate.now().toString());
        review.setUpdatedAt(LocalDate.now().toString());
        game.getReviews().add(review);
        return marketRepository.addReview(gameId, game);
    }

    public void deleteReview(String gameId, String reviewId) {
        Game game = marketRepository.getGameById(gameId);
        Review review = game.getReviews().stream().filter(r -> r.getReviewId().equals(reviewId)).findFirst().get();
        game.getReviews().remove(review);
        marketRepository.deleteReview(gameId, game);
    }

    public Review getReviewById(String gameId, String reviewId) {
        return marketRepository.getReviewById(gameId, reviewId);
    }

    public void updateReview(JsonPatch patch, String gameId, String reviewId) {
        Game game = marketRepository.getGameById(gameId);
        Review reviewById = game.getReviewById(reviewId);
        try {
            Review review = applyPatch(patch, reviewById);
            game.replaceReviewById(review, reviewById);
            marketRepository.updateGame(game);
        } catch (JsonPatchException e) {
            e.printStackTrace();
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public int getUserRating(String gameId) {
        return marketRepository.getUserRating(gameId);
    }

    public List<Review> getReviewsByUserName(String gameId, String userName) {
        return marketRepository.getReviewsByUserName(gameId, userName);
    }

    public void buyGame(String gameId, String userId) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<String> entity = new HttpEntity<String>(headers);
        Double balanceOfUser = getRestTemplate.exchange("http://localhost:8081/user/" + userId + "/balance",
                HttpMethod.GET, entity, Double.class).getBody();
        Game game = marketRepository.getGameById(gameId);
        if (balanceOfUser >= game.getPrice()) {
            game.setSalesCount(game.getSalesCount() + 1);
            marketRepository.updateGame(game);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userId", userId);
            jsonObject.put("gameId", gameId);
            jsonObject.put("gameName", game.getGameName());
            jsonObject.put("price", game.getPrice());
            // sender.sendMessage(jsonObject.toString(), "userBuyGame");
        } else {
            throw new BalanceIsNotEnoughException("Balance is not enough.");
        }
    }

    public void createAnnouncement(String gameId, Announcement announcement) {
        marketRepository.getGameById(gameId);

        JSONObject announcementJson = new JSONObject();
        announcementJson.put("gameId", gameId);
        announcementJson.put("title", announcement.getTitle());
        announcementJson.put("message", announcement.getMessage());
        // sender.sendMessage(announcementJson.toString(), "announcement");
    }

    public void setPriceByGameId(String gameId, JsonPatch patch) throws JsonPatchException, JsonProcessingException {
        Game game = marketRepository.getGameById(gameId);
        try {
            Game gamePatched = this.applyPatch(patch, game);
            marketRepository.updateGame(gamePatched);
        } catch (Exception e) {

        }
    }

    public void updateGame(String gameId, JsonPatch patch) throws JsonPatchException, JsonProcessingException {
        Game game = marketRepository.getGameById(gameId);
        try {
            Game gamePatched = this.applyPatch(patch, game);
            String date = gamePatched.getReleaseDate();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date d = format.parse(date);
            System.out.println(d.toString());
            marketRepository.updateGame(gamePatched);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private <T extends Object> T applyPatch(JsonPatch patch, T target) throws
            JsonPatchException, JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
        JsonNode patched = patch.apply(objectMapper.convertValue(target, JsonNode.class));
        return (T) objectMapper.treeToValue(patched, target.getClass());
    }

    public List<TopGameResponse> getTrendingGames(int offset, int limit) {
        return marketRepository.getTrendingGames(offset, limit);
    }

    public List<TopGameResponse> getTopSellerGames(int offset, int limit) {
        return marketRepository.getTopSellerGames(offset, limit);
    }


    public List<TopGameResponse> getNewReleasesGames(int offset, int limit) {
        LocalDate now = LocalDate.now();
        LocalDate beforeAWeek = now.minusWeeks(1);
        return marketRepository.getNewReleasesGames(offset, limit, now.toString(), beforeAWeek.toString());
    }

    public List<UpcomingGameResponse> getUpcomingGames(int offset, int limit) {
        LocalDate now = LocalDate.now();
        LocalDate afterTwoMonths = now.plusMonths(2);
        return marketRepository.getUpcomingGames(offset, limit, now.toString(), afterTwoMonths.toString());
    }
}
