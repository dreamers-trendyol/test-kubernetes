package com.dreamerstrendyol.marketplace.Contracts.Response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TopGameResponse {
    private String gameId ;
    private String gameName;
    private double price;
    private String gameIcon;
    private String[] tags;

}
