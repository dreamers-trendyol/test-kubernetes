package com.dreamerstrendyol.marketplace.Contracts.Request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReviewValidationDTO {
    @NotNull
    public Boolean adviceIt;
    @NotNull
    @Length(min = 3,max = 250)
    public String reviewContent;
}
