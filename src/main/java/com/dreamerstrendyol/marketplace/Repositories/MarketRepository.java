package com.dreamerstrendyol.marketplace.Repositories;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.query.QueryResult;
import com.dreamerstrendyol.marketplace.Contracts.Response.TopGameResponse;
import com.dreamerstrendyol.marketplace.Contracts.Response.UpcomingGameResponse;
import com.dreamerstrendyol.marketplace.Models.Game;
import com.dreamerstrendyol.marketplace.Models.Review;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MarketRepository {

    private final Collection marketCollection;
    private final Cluster couchbaseCluster;


    public MarketRepository(Cluster couchbaseCluster, Collection marketCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.marketCollection = marketCollection;
    }

    public List<Game> getAllGames() {
        String statement = "Select gameId, gameName, languages from market"; //tüm fieldlar eklenecek
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(Game.class);
    }

    public Game getGameById(String gameId) {
        System.out.println("Hello from repository");
        GetResult getResult = marketCollection.get(gameId);
        return getResult.contentAs(Game.class);
    }

    public String createGame(Game game) {
        marketCollection.insert(game.getGameId(), game);
        return game.getGameId();
    }

    public void deleteGame(String gameId) {
        marketCollection.remove(gameId);
    }

    public void updateGame(Game game) {
        marketCollection.replace(game.getGameId(), game);
    }

    public Double getPrice(String gameId) {
        return getGameById(gameId).getPrice();
    }

    public int getSalesCount(String gameId) {
        return getGameById(gameId).getSalesCount();
    }

    public List<Review> getReviews(String gameId) {
        return getGameById(gameId).getReviews();
    }

    public String addReview(String gameId, Game game) {
        marketCollection.replace(gameId, game);
        return gameId;
    }

    public void deleteReview(String gameId, Game game) {
        marketCollection.replace(gameId, game);
    }

    public Review getReviewById(String gameId, String reviewId) {
        String statement = String.format("SELECT r.reviewId,r.userId,r.userName,r.adviceIt,r.reviewContent,r.createdAt,r.updatedAt FROM market AS m UNNEST m.reviews AS r WHERE m.gameId = \"%s\" AND r.reviewId = \"%s\";", gameId, reviewId);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(Review.class).get(0);
    }

    public int getUserRating(String gameId) {
        return getGameById(gameId).getUserRating();
    }

    public List<Review> getReviewsByUserName(String gameId, String userName) {
        String statement = String.format("SELECT r.reviewId,r.userId,r.userName,r.adviceIt,r.reviewContent,r.createdAt,r.updatedAt FROM market AS m UNNEST m.reviews AS r WHERE m.gameId = \"%s\" AND r.userName = \"%s\";", gameId, userName);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(Review.class);
    }

    public List<TopGameResponse> getTrendingGames(int offset, int limit) {
        String statement = String.format("SELECT gameId, gameName, price, gameIcon, tags FROM market ORDER BY userRating DESC OFFSET %d LIMIT %d", offset, limit);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(TopGameResponse.class);
    }

    public List<TopGameResponse> getTopSellerGames(int offset, int limit) {
        String statement = String.format("SELECT gameId, gameName, price, gameIcon, tags FROM market ORDER BY salesCount DESC OFFSET %d LIMIT %d", offset, limit);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(TopGameResponse.class);
    }

    public List<TopGameResponse> getNewReleasesGames(int offset, int limit, String now, String beforeAWeek) {
        String statement = String.format("SELECT gameId, gameName, price, gameIcon, tags FROM market WHERE releaseDate BETWEEN \"%s %%\" AND \"%s %%\" ORDER BY releaseDate DESC OFFSET %d LIMIT %d", beforeAWeek, now, offset, limit);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(TopGameResponse.class);
    }

    public List<UpcomingGameResponse> getUpcomingGames(int offset, int limit, String now, String afterTwoMonths) {
        String statement = String.format("SELECT gameId, gameName, gameIcon, releaseDate, tags FROM market WHERE releaseDate BETWEEN \"%s %%\" AND \"%s %%\" ORDER BY releaseDate ASC OFFSET %d LIMIT %d", now, afterTwoMonths, offset, limit);
        QueryResult queryResult = couchbaseCluster.query(statement);
        return queryResult.rowsAs(UpcomingGameResponse.class);
    }

}
