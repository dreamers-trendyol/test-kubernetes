Request body should like 

```
[
  { "op": "replace", "path": "/gameName", "value": "new game name" },
  { "op": "add", "path": "/hello", "value": ["world"] },
  { "op": "remove", "path": "/gameName" }
]
```
 while `[PATCH]` endpoint used.
